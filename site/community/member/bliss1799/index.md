
```{post} 2023-07-22
:author: "@bliss1799"
:tags: participante
:category: miembros
:language: Español
:location: Colombia
:excerpt: 1
```

# @bliss1799

Hola soy `@bliss1799`! 

Hola, mucho gusto! Mi nombre es Andrés (Bliss), actualmente estoy estudiando ADSO en el Sena y UX con Google.
Hace poco descubrí programar a la velocidad de la luz con Vim y vivo molestando con eso. 

Éste es mi espacio en GuayaHack.


```console
$ git status 
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
```


